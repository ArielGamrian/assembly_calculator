output: test.o
#	ld -m elf_i386 test.o -o test 
	ld -m elf_i386 -dynamic-linker /lib/ld-linux.so.2 -o test -lc test.o
	# gcc -ggdb -m32 test.o -o test
test.o: test.asm
	nasm -f elf32 -F dwarf -g test.asm

clean:
	rm test *.o
