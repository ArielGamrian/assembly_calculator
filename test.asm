

section .data
	output_txt: db "Enter a mathematical exercise -  a ? b:", 10, 0	
	input_fmt: db "%d %c %d", 0
	print_all_fmt: db "arg1 = %d",10,"operator = %c",10,"arg2 = %d", 10, 0
	print_ans_fmt: db "Answer is %d", 10, 0
	arg1: times 4 db 0
	arg2: times 4 db 0
	operator: times 4 db 0
section .bss

section .text

global _start
extern printf
extern scanf


print_all:
	push ebp
	mov ebp, esp		
	mov eax, [arg2]
	push eax
	mov eax, [operator]
	push eax
	mov eax, [arg1]
	push eax
	push print_all_fmt   
	call printf	
	mov esp, ebp
	pop ebp
	ret

_calc_add:
	call calc_add
	jmp exit
calc_add:
	push ebp
	mov ebp, esp
	mov eax, [arg1]
	mov ebx, [arg2]
	add eax, ebx
	push eax
	push print_ans_fmt
	call printf
	mov esp, ebp
	pop ebp
	ret


_calc_sub:
	call calc_sub
	jmp exit
calc_sub:
	push ebp
	mov ebp, esp
	mov eax, [arg1]
	mov ebx, [arg2]
	sub eax, ebx
	push eax
	push print_ans_fmt
	call printf
	mov esp, ebp
	pop ebp
	ret


_calc_mul:
	call calc_mul
	jmp exit
calc_mul:
	push ebp
	mov ebp, esp
	mov eax, [arg1]
	mov ebx, [arg2]
	imul ebx
	push eax
	push print_ans_fmt
	call printf
	mov esp, ebp
	pop ebp
	ret


_calc_div:
	call calc_div
	jmp exit
calc_div:
	push ebp
	mov ebp, esp
	mov eax, [arg1]
	mov ebx, [arg2]
	mov edx, 0
	div ebx
	push eax
	push print_ans_fmt
	call printf
	mov esp, ebp
	pop ebp
	ret

_start:	           
	
	push output_txt
	call printf
	add esp, 4
	
	push arg2
	push operator
	push arg1
	push input_fmt
	call scanf
	add esp, 16
	
	call print_all

	mov eax,[operator]
	cmp eax, '+'
	je _calc_add

	cmp eax, '-'
	je _calc_sub

	cmp eax, '*'
	je _calc_mul

	cmp eax, '/'
	je _calc_div

exit:
	mov eax, 1
	xor ebx, ebx
	int 0x80
